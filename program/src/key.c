#include "key.h"
#include "delay.h"
#include "lcd.h"
#include <reg51.h>
#include "jisuan.h"
//#include "charcode.h"
uchar array[16]={":"};
extern uchar code cc[17][16];
void keyscan()
{
	uchar temp=0;int i=50000;
	static int hang=1;
	static int lie=1;
	int a=0;
	keyport=0xf0;
	if(keyport!=0xf0)
	{
		delayms(20);
		if(keyport!=0xf0)
		{
			keyport=0xf0;
			switch(keyport)
			{
				case 0x70:a=13;break;
				case 0xb0:a=9;break;
				case 0xd0:a=5;break;
				case 0xe0:a=1;break;
			}
			keyport=0x0f;
			switch(keyport)
			{
				case 0x07:a+=0;break;
				case 0x0b:a+=1;break;
				case 0x0d:a+=2;break;
				case 0x0e:a+=3;break;
			}
		}
		while((keyport!=0x0f)&&(i--));
		
/************************************************************
		以上16个按键会dedao1~16，不按将会返回a=0！
		         以下为按键返回值重置区
************************************************************/
		switch(a)
		{
			case(0):a=255;break;
			case(1):a=0;break;
			case(2):a=-2;break;
			case(3):a=13;break;
			case(4):a=-5;break;
			case(5):a=1;break;
			case(6):a=2;break;
			case(7):a=3;break;
			case(8):a=-3;break;
			case(9):a=4;break;
			case(10):a=5;break;
			case(11):a=6;break;
			case(12):a=-6;break;
			case(13):a=7;break;
			case(14):a=8;break;
			case(15):a=9;break;
			case(16):a=-1;break;
		}
		switch(a)
		{
			case(0):   temp=0; break;
			case(1):   temp=1; break;
			case(2):   temp=2; break;
			case(3):   temp=3; break;
			case(4):   temp=4; break;
			case(5):   temp=5; break;
			case(6):   temp=6; break;
			case(7):   temp=7; break;
			case(8):   temp=8; break;
			case(9):   temp=9; break;
			
			case(-5) : temp=10;break;
			case(-3) : temp=11;break;
			case(-6) : temp=12;break;
			case(-1) : temp=13;break;
			case(13) : temp=14;break;
			case(-2) : temp=15;break;
		}
			
/************************************************************
	                	编程区上界
************************************************************/
		
		write_smchar(lie,hang,cc[temp]);
		lie++;
		if((temp>9)&&(temp<15))
		{
			hang+=2;
		  lie=1;
		}
		jisuan(a,array);
/************************************************************
	                	编程区下界
************************************************************/		
	}

}