#ifndef _lcd_h_
#define _lcd_h_


#include"delay.h"

sbit lcdcs=P2^7;//硬件
sbit reset=P2^6;//硬件
sbit lcdrw=P2^5;//硬件
sbit lcdrd=P2^4;//硬件
sbit lcdrs=P2^3;//硬件

//sbit lcdcs=P3^2;//普中
//sbit reset=P3^3;//普中
//sbit lcdrw=P2^7;//普中
//sbit lcdrd=P2^5;//普中
//sbit lcdrs=P2^6;//普中

void write_com(uchar com);
void write_data(uchar date);
void init();
void clean();
uchar char_swap(uchar dat);
void write_char(uchar x,uchar y,uchar *dat);
void write_smchar(uchar x,uchar y,uchar *dat);

	
#endif